#!/bin/bash

# Assign arguments to variables
PROJECT_SLUG=$1
DEPLOY_TYPE=$2

# Get the directory of the currently executing script
SCRIPT_DIR=$(dirname "$0")

# Source the required scripts
source "$SCRIPT_DIR/staging/helper.sh"
source "$SCRIPT_DIR/staging/variables.sh"

# Check necessary variables
check_variable "$PROJECT_SLUG" "PROJECT_SLUG"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"

if [ -f "$PROJECT_CONFIG_FILE" ]; then
  SSHFS_MOUNTS=$(shyaml -q get-values sshfs-mounts < "$PROJECT_CONFIG_FILE" | sed -z "s/\n/,/g;s/,$/\n/") || error_exit "Failed to get SSHFS_MOUNTS from PROJECT_CONFIG_FILE"

  for FOLDER_NAME in ${SSHFS_MOUNTS//,/ }; do
    echo "Unmount $PROJECT_FOLDER/$FOLDER_NAME"
    sudo umount -l "$PROJECT_FOLDER/$FOLDER_NAME" || true
  done
fi

# Delete all ddev data, unlist project and remove the project folder
echo "Delete, stop and unlist ddev container $PROJECT_SLUG"
ddev stop --unlist "$PROJECT_SLUG" || true && \
ddev delete --omit-snapshot --yes "$PROJECT_SLUG" || true && \
if is_safe_to_delete "$PROJECT_FOLDER"; then
  sudo rm -Rf "$PROJECT_FOLDER"
else
  echo "Error: Unsafe path for deletion: $PROJECT_FOLDER"
  exit 1
fi
