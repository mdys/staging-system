#!/bin/bash

echo "Running scripts/staging/finisher.sh"

# Error handling: Check if ddev is installed
command -v ddev >/dev/null 2>&1 || error_exit "ddev is not installed. Aborting."

# Output all needed information of the created ddev container to gitlab runner log.
ddev describe || error_exit "ddev describe failed. Aborting."

echo "Deployment done!"
