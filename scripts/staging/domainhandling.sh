#!/bin/bash

echo "Running scripts/staging/domainhandling.sh"

# Check necessary variables
check_variable "$PROJECT_SLUG" "PROJECT_SLUG"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"
check_variable "$STAGING_DOMAIN" "STAGING_DOMAIN"

if [ "${DEPLOY_TYPE}" != "live" ]; then
  check_variable "$CI_ENVIRONMENT_SLUG" "CI_ENVIRONMENT_SLUG"
fi

# Initialize ALL_DDEV_DOMAINS
ALL_DDEV_DOMAINS=""

# Append list of additional_hostnames to $ADDITIONAL_HOSTNAMES
if [ -n "${ADDITIONAL_HOSTNAMES}" ]; then
  echo "ADDITIONAL_HOSTNAMES is set with ${ADDITIONAL_HOSTNAMES}"
  # Prepare all domains for usage in preview and live environment
  for DOMAIN in ${ADDITIONAL_HOSTNAMES//,/ }; do
    if [ "${DEPLOY_TYPE}" == "live" ]; then
      DDEV_DOMAIN="${DOMAIN}"
    else
      # Build correct domains for additional hostname during review
      DDEV_DOMAIN="${DOMAIN}.${CI_ENVIRONMENT_SLUG}.${STAGING_DOMAIN}"
    fi

    ALL_DDEV_DOMAINS="${ALL_DDEV_DOMAINS},${DDEV_DOMAIN}"
  done

  # Remove leading comma
  ALL_DDEV_DOMAINS=${ALL_DDEV_DOMAINS:1}
else
    if [ "${DEPLOY_TYPE}" == "live" ]; then
      ALL_DDEV_DOMAINS="${PROJECT_SLUG}.typodreist.de"
    else
      ALL_DDEV_DOMAINS="${PROJECT_SLUG}.${CI_ENVIRONMENT_SLUG}.${STAGING_DOMAIN}"
    fi
fi

echo "Domain preparations are done. Using the following domains: ${ALL_DDEV_DOMAINS}"
