#!/bin/bash

# Common functions for all scripts

# Logging function
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" | tee -a deployment.log
}

# Error handling function
error_exit() {
    log "ERROR: $1"
    exit 1
}

# Function to check if a variable is set and not empty
check_variable() {
    local var_value="$1"
    local var_name="$2"
    if [ -z "$var_value" ]; then
        error_exit "Variable $var_name not set or empty"
    fi
}

# Function to create a slug from a project name
create_slug() {
    echo "$1" | sed -E 's/[^[:alnum:]]+/-/g'
}

# Function to check if a path is safe to delete
is_safe_to_delete() {
  local path=$1
  if [[ -z "$path" || "$path" == "/" || "$path" == "~" || "$path" == "$HOME" || "$path" == "/root" ]]; then
    return 1
  else
    return 0
  fi
}

# Function to check if a database exists
database_exists() {
  local db_name="$1"
  if mysql -e "use ${db_name}"; then
    return 0
  else
    return 1
  fi
}

# Function to return a string of asterisks the same length as the input string
show_hidden_values() {
    local input_string="$1"
    local hidden_string=""
    for ((i=0; i<${#input_string}; i++)); do
        hidden_string+="*"
    done
    echo "$hidden_string"
}
