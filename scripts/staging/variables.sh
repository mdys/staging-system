#!/bin/bash

# Check necessary variables
check_variable "$PROJECT_SLUG" "PROJECT_SLUG"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"

if [ "$DEPLOY_TYPE" == "live" ]; then
  MYSQL_SERVER_USER=$(sed -n '/^user /{s/.*= *//p;q}' /home/ddev/.my.cnf) || error_exit "Failed to get MYSQL_SERVER_USER"
  MYSQL_SERVER_IP=$(sed -n '/^host /{s/.*= *//p;q}' /home/ddev/.my.cnf) || error_exit "Failed to get MYSQL_SERVER_IP"
  MYSQL_SERVER_PORT=$(sed -n '/^port /{s/.*= *//p;q}' /home/ddev/.my.cnf) || error_exit "Failed to get MYSQL_SERVER_PORT"
  MYSQL_SERVER_PASSWORD=$(sed -n '/^password /{s/.*= *//p;q}' /home/ddev/.my.cnf) || error_exit "Failed to get MYSQL_SERVER_PASSWORD"
  MOUNT_OPTIONS="allow_other,cache=yes,kernel_cache,compression=no,reconnect,ServerAliveInterval=15,ServerAliveCountMax=3"
else
  MOUNT_OPTIONS="allow_other,ro,cache=yes,kernel_cache,compression=no,reconnect,ServerAliveInterval=15,ServerAliveCountMax=3"
fi

PROJECT_FOLDER="/home/ddev/$PROJECT_SLUG"
DDEV_CONFIG_FILE="$PROJECT_FOLDER/.ddev/config.yaml"
PROJECT_CONFIG_FILE="$PROJECT_FOLDER/.ddev/project.yaml"

PROJECT_NAME=$(shyaml -q get-value name < "$DDEV_CONFIG_FILE")
PROJECT_NAME_SLUG=$(create_slug "$PROJECT_NAME")
SSH_PROJECT_NAME_SLUG="$PROJECT_NAME_SLUG"
DATABASE_NAME=$(echo "$PROJECT_NAME" | sed -E 's/[^[:alnum:]]+/_/g')

# Check if force-projectname is set and replace $PROJECT_NAME with value from force-projectname
if [ -f "$PROJECT_CONFIG_FILE" ]; then
  FORCE_PROJECT_NAME=$(shyaml -q get-value force-projectname < "$PROJECT_CONFIG_FILE")
  NO_DATABASE=$(shyaml -q get-value no-database < "$PROJECT_CONFIG_FILE")
  NO_DB_IMPORT_FROM_LIVE=$(shyaml -q get-value no-db-import-from-live < "$PROJECT_CONFIG_FILE")

  if [ -n "$FORCE_PROJECT_NAME" ]; then
    echo "force-projectname is set. Using $FORCE_PROJECT_NAME instead of $PROJECT_NAME."
    DATABASE_NAME=$(echo "$FORCE_PROJECT_NAME" | sed -E 's/[^[:alnum:]]+/_/g')
    SSH_PROJECT_NAME_SLUG=$(create_slug "$FORCE_PROJECT_NAME")
  fi
fi

# Get values from ddev config file which is in YAML format using shyaml.
if [ -f "$DDEV_CONFIG_FILE" ]; then
  ADDITIONAL_HOSTNAMES=$(shyaml -q get-values additional_hostnames < "$DDEV_CONFIG_FILE" | sed -z "s/\n/,/g;s/,$/\n/") || error_exit "Failed to get ADDITIONAL_HOSTNAMES from DDEV_CONFIG_FILE"
  DDEV_TYPE=$(shyaml -q get-value type < "$DDEV_CONFIG_FILE") || error_exit "Failed to get DDEV_TYPE from DDEV_CONFIG_FILE"
  DDEV_DOCROOT=$(shyaml -q get-value docroot < "$DDEV_CONFIG_FILE") || error_exit "Failed to get DDEV_DOCROOT from DDEV_CONFIG_FILE"
fi

echo "Variables set inside variables.sh"
echo "PROJECT_FOLDER: $PROJECT_FOLDER"
echo "DDEV_CONFIG_FILE: $DDEV_CONFIG_FILE"
echo "PROJECT_CONFIG_FILE: $PROJECT_CONFIG_FILE"
echo "MOUNT_OPTIONS: $MOUNT_OPTIONS"
echo "NO_DATABASE: $NO_DATABASE"
echo "NO_DB_IMPORT_FROM_LIVE: $NO_DB_IMPORT_FROM_LIVE"
echo "DATABASE_NAME: $DATABASE_NAME"
echo "SSH_PROJECT_NAME_SLUG: $SSH_PROJECT_NAME_SLUG"
echo "FORCE_PROJECT_NAME: $FORCE_PROJECT_NAME"
echo "ADDITIONAL_HOSTNAMES: $ADDITIONAL_HOSTNAMES"
echo "DDEV_TYPE: $DDEV_TYPE"
echo "DDEV_DOCROOT: $DDEV_DOCROOT"
echo "MYSQL_SERVER_USER: $(show_hidden_values "$MYSQL_SERVER_USER")"
echo "MYSQL_SERVER_IP: $(show_hidden_values "$MYSQL_SERVER_IP")"
echo "MYSQL_SERVER_PORT: $(show_hidden_values "$MYSQL_SERVER_PORT")"
echo "MYSQL_SERVER_PASSWORD: $(show_hidden_values "$MYSQL_SERVER_PASSWORD")"
