# Using Values in .ddev/project.yaml

The `.ddev/project.yaml` file is essential for configuring various aspects of
your DDEV environment. Below are the key
values that you can define in this file, along with their descriptions and
usage:

## Configuration Values

- **no-database**
  - **Description**: Specifies that no database should be used in the
    environment.
  - **Usage**: Set this to `true` if your project does not require a database.

- **force-projectname**
  - **Description:** Forces the use of a specific project name. This can be
    useful if you want to standardize project
    naming conventions across environments.
  - **Usage:** Define the desired project name.

- **http-auth-staging**
  - **Description:** Enables HTTP authentication for the staging environment.
  - **Usage:** Set this to `true` to enable HTTP authentication in the staging
    environment.

- **http-auth-staging-username**
  - **Description:** Sets the username for HTTP authentication in staging.
  - **Usage:** Define the username to be used for HTTP authentication.

- **http-auth-staging-password**
  - **Description:** Sets the password for HTTP authentication in staging.
  - **Usage:** Define the password to be used for HTTP authentication.

- **sshfs-mounts**
  - **Description:** Specifies directories to be mounted using SSHFS.
  - **Usage:** List the directories to be mounted in an array format.

- **secure_files**
  - **Description:** Lists files that need to be securely transferred, such as
    environment files and configuration
    files.
  - **Usage:** List the files to be transferred securely.

- **no_db_import_from_live**
  - **Description:** Do not import the database from the live environment but
    keep using the database in the review
    environment.
  - **Usage:** Set this to `true` if you do not want to import the live database
    into the review environment.

## Example Configuration

```yaml
no-database: true
force-projectname: myproject
http-auth-staging: true
http-auth-staging-username: staginguser
http-auth-staging-password: stagingpassword
sshfs-mounts: [ public/fileadmin,src/some/files ]
secure-files: .env,.special_config,password.txt
no-db-import-from-live: true
```
